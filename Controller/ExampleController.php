<?php

namespace Nilopc\PaginationBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Nilopc\PaginationBundle\Pagination;
use Nilopc\PaginationBundle\Adapter\DoctrineOrmAdapter;
use Nilopc\PaginationBundle\Adapter\ArrayAdapter;

/**
 * ExampleController
 * 
 * @Route("/_example")
 */
class ExampleController extends Controller
{

  /**
     * Pagination with doctrine
     *
     * @Route("/doctrine/ex1/{page}", defaults={"page"=1}, name="pagination_doctrine1")
     * @Template("NilopcPaginationBundle:Example:base.html.twig")
     */
	public function doctrineExample1Action($page, Request $request)
	{
		$em = $this->getDoctrine()->getEntityManager();
		$query = $em->getRepository('NilopcWorldCitiesBundle:Country')->createQueryBuilder('m');
	
		$adapter = new DoctrineOrmAdapter($query);
		$pagination = new Pagination($adapter,array('page' => $page, 'limit' => 25));
		return array('pagination' => $pagination);
	}


  /**
     * Pagination with doctrine
     *
     * @Route("/doctrine/ex2/{page}", defaults={"page"=1}, name="pagination_doctrine2")
     * @Template("NilopcPaginationBundle:Example:base.html.twig")
     */
	public function doctrineExample2Action($page, Request $request)
	{
		$em = $this->getDoctrine()->getEntityManager();

    	$query = $em->createQueryBuilder();
	    $query->add('select', 'm')
	       	  ->add('from', 'NilopcWorldCitiesBundle:Country m')
	          ->add('orderBy', 'm.name DESC');


		$adapter = new DoctrineOrmAdapter($query);
		$pagination = new Pagination($adapter,array('page' => $page, 'limit' => 25));
		return array('pagination' => $pagination);
	}


  /**
     * Pagination with an array
     *
     * @Route("/array/{page}", defaults={"page"=1}, name="pagination_array")
     * @Template("NilopcPaginationBundle:Example:base.html.twig")
     */
	public function arrayExampleAction($page, Request $request)
	{
		$array = range(1, 100);
		$adapter = new ArrayAdapter($array);
		$pagination = new Pagination($adapter, array('page' => $page, 'limit' => 25));

		return array('pagination' => $pagination);		
	}	
}