=================================================================
Installation
=================================================================
 - Copy the PaginationBundle to /src/Nilopc/PaginationBundle
 - Register the bundle in your AppKernel
 - Run "./app/console assets:install web" command
 - You are done


=================================================================
USAGE: Controller
=================================================================

DOCTRINE:

	<?php
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
	use Nilopc\PaginationBundle\Pagination;
	use Nilopc\PaginationBundle\Adapter\DoctrineOrmAdapter;
	
	class ExampleController extends Controller
	{
		public function indexAction( $page, Request $request )
		{
			$em = $this->getDoctrine()->getEntityManager();
			$qb = $em->getRepository('NilopcWorldCitiesBundle:Country')->createQueryBuilder('m');
		
			$adapter = new DoctrineOrmAdapter($qb);
			$pagination = new Pagination($adapter, array('page' => $page, 'limit' => 25));
			return array('pagination' => $pagination);
		}
	}	
	?>

Or even like this

	<?php
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
	use Nilopc\PaginationBundle\Pagination;
	use Nilopc\PaginationBundle\Adapter\DoctrineOrmAdapter;
	
	class ExampleController extends Controller
	{	
		public function doctrineExample2Action($page, Request $request)
		{
			$em = $this->getDoctrine()->getEntityManager();

	    	$query = $em->createQueryBuilder();
		    $query->add('select', 'm')
		       	  ->add('from', 'NilopcWorldCitiesBundle:Country m')
		          ->add('orderBy', 'm.name DESC');


			$adapter = new DoctrineOrmAdapter($query);
			$pagination = new Pagination($adapter,array('page' => $page, 'limit' => 25));
			return array('pagination' => $pagination);
		}
	}	
	?>

ARRAY:

	<?php
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
	use Nilopc\PaginationBundle\Pagination;
	use Nilopc\PaginationBundle\Adapter\ArrayAdapter;

	class ExampleController extends Controller
	{
	   /**
	    *
	    * @Route("/example/{page}", defaults={"page"=1}, name="example_route")
	    * @Template()
	    */
	   public function exampleAction($page)
	   {
	      $array = range(1, 100);
	      $adapter = new ArrayAdapter($array);
	      $pagination = new Pagination($adapter, array('page' => $page, 'limit' => 25));

	      return array('pagination' => $pagination);
	   }
	}

=================================================================
USAGE: Twig template
=================================================================

	{% if pagination.isPaginable %}
	   {{ paginate(pagination, 'example_route') }}
	{% endif %}
	{% for item in pagination.getResults %}
	   <p>{{ item }}</p>
	{% endfor %}

	-----------------------------------------------------------------
	PHP template
	-----------------------------------------------------------------
	<?php if ($pagination->isPaginable()): ?>
	    <?php echo $view['Pagination']->paginate($pagination, 'example_route') ?>
	<?php endif; ?>
	<?php foreach ($pagination->getResults() as $item): ?>
	    <p><?php echo $item ?></p>
	<?php endforeach; ?>

	-----------------------------------------------------------------
	Default CSS styles
	-----------------------------------------------------------------
	There are two default css styles located in web/bundles/NilopcPagination/css/ (clean.css and round.css). You must include them manually in the layout template.

=================================================================
USAGE: Adapters
=================================================================

	-----------------------------------------------------------------
	ArrayAdapter
	-----------------------------------------------------------------

	<?php
	use Nilopc\PaginationBundle\Adapter\ArrayAdapter;
	/* ... */
	$adapter = new ArrayAdapter($your_array);



	-----------------------------------------------------------------
	DoctrineOrmAdapter
	-----------------------------------------------------------------

	<?php
	use Nilopc\PaginationBundle\Adapter\DoctrineOrmAdapter;
	/* ... */
	$em = $this->getDoctrine()->getEntityManager();               
	$qb = $em->getRepository('ExampleEntity')->createQueryBuilder('f');
	$adapter = new DoctrineOrmAdapter($qb);



=================================================================
Customization
=================================================================

	-----------------------------------------------------------------
	Passing additional parameters to the Pagination links
	-----------------------------------------------------------------
	You can pass as many additional parameters as you will fit. For example lets modify our route example_route and pass an additional {type} parameter:

	@Route("/example/{page}/{type}", defaults={"page"=1}, name="example_route")

		-----------------------------------------------------------------
		 Twig template:
		-----------------------------------------------------------------
		{{ paginate(pagination, 'example_route', {'type': 'long'}) }}

		-----------------------------------------------------------------
		 PHP template:
		-----------------------------------------------------------------
		<?php echo $view['pagination']->paginate($pagination, 'example_route', array('type': 'long')) ?>

	-----------------------------------------------------------------
	Changing the default 'page' parameter name
	-----------------------------------------------------------------
	For example lets modify our route example_route and change the default {page} parameter to {offset}:

	@Route("/example/{offset}", defaults={"offset"=1}, name="example_route")

		-----------------------------------------------------------------
		 Twig template:
		-----------------------------------------------------------------
		{{ paginate(pagination, 'example_route', {'_page': 'offset'}) }}


		-----------------------------------------------------------------
		 PHP template:
		-----------------------------------------------------------------
		<?php echo $view['Pagination']->paginate($pagination, 'example_route', array('_page': 'offset')) ?>



	-----------------------------------------------------------------
	Modifying the default rendering template
	-----------------------------------------------------------------
	There are 2 default rendering templates located in PaginationBundle/Resources/views/Pagination:

	 - Twig: paginate.html.twig
	 - PHP: paginage.html.php

	If you want to customize their HTML simply copy one of them to /app/Resources/NilopcPaginationBundle/views/Pagination

	Alternatively you can pass the name of your template (in bundle:section:template.format.engine format) to the paginate helper:

	-----------------------------------------------------------------
	Twig template:
	-----------------------------------------------------------------
	{{ paginate(Pagination, 'example_route', {}, 'ExampleBundle:Example:name.html.twig') }}

	-----------------------------------------------------------------
	PHP template:
	-----------------------------------------------------------------
	<?php echo $view['Pagination']->paginate($pagination, 'example_route', array(), 'ExampleBundle:Example:name.html.php') ?>


