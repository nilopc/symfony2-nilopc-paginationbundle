<?php

/*
 * This file is part of the PaginationBundle package.
 *
 *
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilopc\PaginationBundle\Twig\Extension;

use Nilopc\PaginationBundle\Pagination;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * PaginationExtension extends Twig with pagination capabilities.
 *
 * 
 */
class PaginationExtension extends \Twig_Extension
{
    /**
     *
     * @var RouterInterface
     */
    protected $router;
    /**
     *
     * @var \Twig_Environment
     */
    protected $environment;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function initRuntime(\Twig_Environment $environment)
    {
        $this->environment = $environment;
    }

    public function getFunctions()
    {
        return array(
            'paginate' => new \Twig_Function_Method($this, 'paginate', array('is_safe' => array('html'))),
            'paginate_path' => new \Twig_Function_Method($this, 'path', array('is_safe' => array('html'))),
        );
    }

    public function paginate(Pagination $pagination, $route, array $parameters = array(), $template = 'NilopcPaginationBundle:Pagination:paginate.html.twig')
    {
        return $this->environment->render($template, array('pagination' => $pagination, 'route' => $route, 'parameters' => $parameters));
    }

    public function path($route, $page, array $parameters = array())
    {
        if (isset($parameters['_page'])) {
            $parameters[$parameters['_page']] = $page;

            unset($parameters['_page']);
        } else {
            $parameters['page'] = $page;
        }

        return $this->router->generate($route, $parameters);
    }

    public function getName()
    {
        return 'pagination';
    }
}
