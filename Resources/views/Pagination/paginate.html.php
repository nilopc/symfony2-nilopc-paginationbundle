<ul class="pagination">
    <?php if ($pagination->isFirstPage() == false): ?>    
        <li class="first"><a href="<?php echo $view['pagination']->path($route, $pagination->getFirstPage(), $parameters) ?>">&laquo;</a></li>
        <li class="previous"><a href="<?php echo $view['pagination']->path($route, $pagination->getPreviousPage(), $parameters) ?>">&lsaquo;</a></li>
    <?php endif ?>
    <?php foreach ($pagination->getPages() as $page): ?>
        <?php if ($page == $pagination->getPage()): ?>
            <li class="selected">
                <b><?php echo $page ?></b>
            </li>      
        <?php else: ?>
            <li>
                <a href="<?php echo $view['pagination']->path($route, $page, $parameters) ?>"><?php echo $page ?></a>
            </li>
        <?php endif ?>
    <?php endforeach; ?>  
    <?php if ($pagination->isLastPage() == false): ?>
        <li class="next"><a href="<?php echo $view['pagination']->path($route, $pagination->getNextPage(), $parameters) ?>">&rsaquo;</a></li>
        <li class="last"><a href="<?php echo $view['pagination']->path($route, $pagination->getLastPage(), $parameters) ?>">&raquo;</a></li>
    <?php endif ?>
</ul>
