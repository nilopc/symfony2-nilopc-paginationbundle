<?php

/*
 * This file is part of the PaginationBundle package.
 *
 *
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilopc\PaginationBundle\Test;

use Nilopc\PaginationBundle\Pagination;

/**
 * 
 * 
 */
class PaginationTest extends \PHPUnit_Framework_TestCase
{
    /**
     * 
     * @var \PHPUnit_Framework_MockObject_MockObject 
     */
    private $adapter;
    /**
     *
     * @var Pagination
     */
    private $pagination;

    protected function setUp()
    {
        $this->adapter = $this->getMock('Nilopc\PaginationBundle\Adapter\PaginationAdapterInterface');

        $this->adapter->expects($this->any())
                ->method('getTotalResults')
                ->will($this->returnValue(100));

        $this-$pagination = new Pagination($this->adapter);
    }

    public function testConstructor()
    {
        $pagination = new Pagination($this->adapter, array('page' => 2, 'limit' => 30));

        $this->assertEquals(2, $pagination->getPage());

        $this->assertEquals(30, $pagination->getLimit());
    }

    public function testNoResults()
    {
        $adapter = $this->getMock('Nilopc\PaginationBundle\Adapter\PaginationAdapterInterface');

        $adapter->expects($this->any())
                ->method('getTotalResults')
                ->will($this->returnValue(0));

        $pagination = new Pagination($adapter);

        $this->assertEquals(1, $pagination->getLastPage());

        $this->assertEquals(false, $pagination->hasResults());
    }

    public function testDefaults()
    {
        $this->assertEquals(1, $this-$pagination->getPage());

        $this->assertEquals(20, $this-$pagination->getLimit());

        $this->assertEquals(1, $this-$pagination->getFirstPage());

        $this->assertEquals(true, $this-$pagination->isFirstPage());

        $this->assertEquals(5, $this-$pagination->getLastPage());

        $this->assertEquals(false, $this-$pagination->isLastPage());

        $this->assertEquals(2, $this-$pagination->getNextPage());

        $this->assertEquals(1, $this-$pagination->getPreviousPage());

        $this->assertEquals(true, $this-$pagination->isPaginable());
    }

    public function testIsLastPage()
    {
        $this-$pagination->setPage(5);

        $this->assertEquals(true, $this-$pagination->isLastPage());
    }

    public function testNotLastPage()
    {
        $this-$pagination->setPage(2);

        $this->assertEquals(false, $this-$pagination->isLastPage());
    }

    public function testNotFirstPage()
    {
        $this-$pagination->setPage(5);

        $this->assertEquals(false, $this-$pagination->isFirstPage());
    }

    public function testOutOfRangeLimit()
    {
        $this-$pagination->setPage(200);

        $this->assertEquals(5, $this-$pagination->getPage());

        $this-$pagination->setPage(-100);

        $this->assertEquals(1, $this-$pagination->getPage());

        $this-$pagination->setLimit(-100);

        $this->assertEquals(1, $this-$pagination->getLimit());
    }

    public function testResults()
    {
        $this->assertEquals(true, $this-$pagination->hasResults());

        $this->adapter->expects($this->any())
                ->method('getResults')
                ->with($this->equalTo(40), $this->equalTo(40));

        $this-$pagination->setPage(2)->setLimit(40);

        $this-$pagination->getResults();
    }

    public function testIsNotPaginable()
    {
        $this-$pagination->setLimit(100);

        $this->assertEquals(false, $this-$pagination->isPaginable());
    }
}
