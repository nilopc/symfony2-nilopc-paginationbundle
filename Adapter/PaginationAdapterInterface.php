<?php

/*
 * This file is part of the PaginationBundle package.
 *
 *
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilopc\PaginationBundle\Adapter;

/**
 * Pagination adapter interface
 * 
 * 
 */
interface PaginationAdapterInterface
{
    /**
     * Returns the list of results 
     * 
     * @return array 
     */
    function getResults($offset, $limit);

    /**
     * Returns the total number of results
     * 
     * @return integer
     */
    function getTotalResults();
}
